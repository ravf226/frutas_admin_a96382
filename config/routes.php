<?php 

include_once(dirname(__FILE__) . '/../configs.php');
foreach (glob(dirname(__FILE__) . '/../controllers/*.php') as $filename) {
	include_once $filename;
}

class Router {

	public function route($action = 'logout') {
       switch ($action) {
         case 'login':
              if((isset($_POST['user_name']) && $_POST['user_name'] != '') && (isset($_POST['user_pass']) && $_POST['user_pass'] != '')) {
                $login = new LoginController(true);  
                $login->validar_usuario($_POST['user_name'], $_POST['user_pass']);
              }
              else {
                $m = array('error' => 'Le ha faltado llenar informacion');
                $login = new LoginController(false, $m);
                // Mandar mensaje de error de incompletitud de campos
               }
            break;
         case 'inventario':
             $inventario = new InventarioController();
             break;
         case 'compras':
             $c = new ComprasController();
             break;
         case 'ventas':
             $v = new VentasController();
             break;
         case 'comprar_frutas':
           $c = new FrutasController();
           $c->redirect_to_view();
         break;
         case 'procesar_compra_frutas':
           $c = new FrutasController();
           if($_POST['cantidad_fruta'] != '' && $_POST['codigo_fruta'] != '' && $_POST['numero_tarjeta'] != '' && $_POST['venc_tarjeta_year'] != '' && $_POST['venc_tarjeta_mes'] != '' && $_POST['venc_tarjeta_dia'] != '' && $_POST['codigo_tarjeta'] != '') {
              $fecha_venc_tarjeta = '' . $_POST['venc_tarjeta_year'] . '-' . $_POST['venc_tarjeta_mes'] . '-' . $_POST['venc_tarjeta_dia'];
              if($c->procesar_compra($_POST["select_fruta"], $_SESSION['uid'], $_POST['codigo_fruta'], $_POST['desc'], $_POST['cantidad_fruta'], $_POST['numero_tarjeta'], $fecha_venc_tarjeta, $_POST['codigo_tarjeta'])) {
                // se proceso bien la compra
                $compras = new ComprasController();
              }
              else {
                // murio la compra
                $m = array('error' => 'No se pudo procesar bien la compra');
                $c->redirect_to_view($m);
              }
           }
           else {
             $m = array('error' => 'Le ha faltado llenar info como el codigo, cantidad de fruta, datos de tarjeta, etc');
             $c->redirect_to_view($m);
           }
           break;
         case 'logout':
              $sesion_controller = new SesionController();
              $sesion_controller->borrar_sesion_activa(session_id());
              $sesion_controller->clean_all();
              $logout = new LogoutController(true);
             break;
         default:
         	# code...
         	break;
        }
	}
}

$action = '';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        # code...
        $action = $_GET['action'];
        break;
    case 'POST':
        $action = $_POST['action'];
        break;
    default:
        # code...
       $action = 'logout';
        break;
}

$r = new Router();
$r->route($action);

?>
