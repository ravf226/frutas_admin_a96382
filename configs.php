<?php
$config = array(
    'site_url' => 'http://localhost:8000',
    'root' => $_SERVER['DOCUMENT_ROOT'],
    'start_page' => 'views/login.php',
    'error_page' => '404.php',
    'session_lifetime' => 600, //time in seconds
    'test_mode' => true
);
?>