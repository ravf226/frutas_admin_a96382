<?php 
 
include_once(dirname(__FILE__) . '/../configs.php');
include_once(dirname(__FILE__) . '/../models/conexion.php');

class BaseController {

  public function get_view_html() {
    return $GLOBALS['config']['root'] . '/views/' . strtolower(str_replace('Controller', '', get_class($this))) . '.php';
  }

  public function get_view_url() {
  	return $GLOBALS['config']['site_url'] . '/views/' . strtolower(str_replace('Controller', '', get_class($this))) . '.php';
  }


  private function url_origin($s, $use_forwarded_host=false) {
    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
    $sp = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
    $host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host;
  }

  public function full_url($s, $use_forwarded_host=false) {
    return $this->url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
  }

  public function sesion_activa() {

  }

  public function redirect_to_view($mensajes = array()) {
    if(!empty($mensajes)) {
      // llenamos session de mensajes de error
       $_SESSION['mensajes'] = $mensajes;
    }
    header("Location:".$this->get_view_url());
    die();
  }


}

?>
