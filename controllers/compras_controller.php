<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_controller.php');

include_once(dirname(__FILE__) . '/../models/compra.php');

class ComprasController extends BaseController {
   private $compras;

  function __construct() {
    $sesion = new SesionController();
    $this->compras = $this->get_compras();  
    $_SESSION['compras'] = $this->compras;
    header("Location:".$this->get_view_url());
    die();
  }

  public function get_compras() {
  	$c = new Compra();
  	return $c->get_compras();
  }
}

?>
