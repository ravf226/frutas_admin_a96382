<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_controller.php');

include_once(dirname(__FILE__) . '/../models/fruta.php');

class FrutasController extends BaseController {
   private $frutas;

  function __construct() {
    $sesion = new SesionController();
    $this->frutas = $this->get_frutas();  
    $_SESSION['frutas'] = $this->frutas->lista_frutas();
  }

  public function get_frutas() {
  	$c = new Fruta();
  	return $c;
  }

  public function procesar_compra($fruta_info, $usuario_id, $codigo_compra, $descripcion, $cantidad, $num_tarjeta, $fecha_tarjeta, $codigo_tarjeta) {
    $codigo_compra = filter_var($codigo_compra, FILTER_SANITIZE_STRING);
    $descripcion = filter_var($descripcion, FILTER_SANITIZE_STRING);
    $cantidad = filter_var($cantidad, FILTER_SANITIZE_STRING);
    $num_tarjeta = filter_var($num_tarjeta, FILTER_SANITIZE_STRING);
    $fecha_tarjeta = filter_var($fecha_tarjeta, FILTER_SANITIZE_STRING);
    $codigo_tarjeta =  filter_var($codigo_tarjeta, FILTER_SANITIZE_STRING);
    $t = date("Y-m-d", time());
    $fruta_info = filter_var($fruta_info, FILTER_SANITIZE_STRING);
    $fruta_arr = explode('|', $fruta_info);
    $fruta_id = $fruta_arr[0];
    $fruta_monto = floatval($fruta_arr[1]) * floatval($cantidad);
    $info = $num_tarjeta . '|' . $fecha_tarjeta . '|' .$codigo_tarjeta;
    return $this->frutas->insertar_compra($codigo_compra, $t, $descripcion, $usuario_id, $fruta_id, $fruta_monto, $info);
  }
}

?>
