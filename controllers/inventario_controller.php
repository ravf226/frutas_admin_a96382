<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_controller.php');

include_once(dirname(__FILE__) . '/../models/compra.php');

class InventarioController extends BaseController {

  private $inventario;

  function __construct() {
    $this->compras = $this->get_compras();  
    $_SESSION['inventario'] = $this->compras;  
    header("Location:".$this->get_view_url());
    die();
  }

  public function get_compras() {
  	$c = new Compra();
  	return $c->get_inventario();
  }
	 
}

?>
