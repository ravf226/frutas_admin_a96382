<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_controller.php');
include_once('inventario_controller.php');
include_once('sesion_controller.php');
include_once(dirname(__FILE__) . '/../models/usuario.php');

class LoginController extends BaseController {
  
  private $usuario;
  private $mensajes;
  private $sesion;

  function __construct($validar = false, $_mensajes=array()) {
    $this->sesion = new SesionController();
    if(!$this->sesion->sesion_activa($_SESSION['uid'])) {
       $this->mensajes = $_mensajes;
      if(!$validar) {
        $_SESSION['mensajes'] = $this->mensajes;
        $this->sesion->clean_all_but_messages($_SESSION);
        header("Location:".$this->get_view_url());
        die();
      }
      $this->usuario = new Usuario();
    }
    else {
      $_SESSION['mensajes'] = '';
      $inventario = new InventarioController();
    }
  }

  public function validar_usuario($usuario, $pass) {
    $es_valido = $this->usuario->usuario_es_valido($usuario, $pass);
    if($es_valido) {
      // Inserta en Tabla sesion
      $t = date("Y-m-d H:i:s", time());
      $this->sesion->asociar_usuario($this->usuario->get_id());
      $this->sesion->crear_sesion_activa(session_id(), $this->usuario->get_id(), $_SERVER['REMOTE_ADDR'], 0, $t);
      $_SESSION['uid'] = $this->usuario->get_id();
      // redir ecciona a controlador de inventario que es la vista default
      $_SESSION['mensajes'] = '';
      $inventario = new InventarioController();
    }
    else {
      // Redirect to logout
      $this->sesion->clean_all();
      $logout = new LogoutController(true, array('error' => 'Su usuario y password no son validos'));
    }
  }
}


?>
