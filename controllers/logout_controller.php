<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('login_controller.php');
include_once('base_controller.php');


class LogoutController extends BaseController {

  function __construct($forward_to_login=false, $_mensajes=array()) {
    if($forward_to_login) {
      $login_controller = new LoginController(false, $_mensajes);
    }
    if(isset($_SESSION['uid'])) {
      unset($_SESSION['uid']);
    }
    header("Location :" . $this->get_view_url());
    die();   
  }

  public function destroy()
  {

  }
}

?>
