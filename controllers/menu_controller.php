<?php

include_once('base_controller.php');

include_once(dirname(__FILE__) . '/../models/menu.php');

class MenuController extends BaseController {

  private $menu;

  function __construct($is_logged_in) {
      $this->menu = new Menu($is_logged_in);
  }

  function __destruct() {
  	$this->menu = null;
  }

  public function get_menu() {
  	return $this->menu->get_menu_hash();
  }


}

?>
