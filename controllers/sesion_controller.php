<?php

include_once('base_controller.php');

include_once(dirname(__FILE__) . '/../models/sesion.php');


class SesionController extends BaseController {

  private $sesion;

  function __construct() {
    $this->sesion = new Sesion();
  }

  function __destruct() {
  	$this->sesion = null;
  }

  public function is_logged_in() {
  	return $this->sesion->logged_in();
  }

  public function crear_sesion_activa($sesion_id, $usuario_id, $ip, $activa, $time_stamp) {
    $this->sesion->crear_sesion_activa($sesion_id, $usuario_id, $ip, $activa, $time_stamp);
  }

  public function sesion_activa($usuario_id = null) {
    $esta_activa = false;
    if(isset($usuario_id)) {
      $esta_activa = $this->sesion->validar_sesion_activa($usuario_id);
    }
    return $esta_activa;
  }

  public function actualizar_sesion_activa($sesion_id, $usuario_id, $ip, $activa, $time_stamp) {
    $this->sesion->actualizar_sesion_activa($sesion_id, $usuario_id, $ip, $activa, $time_stamp);
  }

  public function borrar_sesion_activa($sesion_id) {
    $this->sesion->borrar_sesion_activa($sesion_id);
  }

  public function asociar_usuario($u_id) {
    $this->sesion->asociar_usuario($u_id);
  }

  public function clean_all_but_messages(&$s) {
    foreach ($s as $key => $value) {
      # code...

      if($key != 'mensajes') {
        unset($s[$key]);
      }
    }

  }

 public function clean_all(&$s) {
    foreach ($s as $key => $value) {
      # code...
        unset($s[$key]);
      
    }

  }


}

?>
