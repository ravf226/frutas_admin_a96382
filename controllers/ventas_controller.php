<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_controller.php');

include_once(dirname(__FILE__) . '/../models/venta.php');


class VentasController extends BaseController {
   private $ventas;

  function __construct() {
    $sesion = new SesionController();
    $this->ventas = $this->get_ventas();  
    $_SESSION['ventas'] = $this->ventas;  
    header("Location:".$this->get_view_url());
    die();
  }

  public function get_ventas() {
  	$v = new Venta();
  	return $v->get_ventas();
  }
}


?>
