<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('conexion.php');


class BaseModel {

  protected $conexion;   

  function __construct() {
    if(!isset($this->conexion)){
      try
      {
        $this->conexion = new Conexion();
      }
      catch(Exception $e)
      {
       echo 'Caught exception in database conexion: ',  $e->getMessage(), "\n";
      }
     }
  }



}
?>
