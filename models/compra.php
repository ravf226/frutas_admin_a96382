<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_model.php');

class Compra extends BaseModel  {

	public function get_compras() {
      $results = array();
      $q = "CALL seleccionar_compras()";
      $result = $this->conexion->recuperar_datos($q);
      while($row = $result->fetch_assoc()){         
        $results[] = $row;
      }
      $result->free();
      $this->conexion->liberar_datos(); 
      return $results;
	}

	public function get_inventario() {
	  $results = array();
    $q = "CALL seleccionar_inventario()";
      
    $result = $this->conexion->recuperar_datos($q);
    while($row = $result->fetch_assoc()){         
      $results[] = $row;
    }
    $result->free();
    $this->conexion->liberar_datos(); 
    return $results;	
	}

}
?>
