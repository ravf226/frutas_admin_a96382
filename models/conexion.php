<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Conexion{
  
  private $conn;
  private $app_user;
  private $ip;
  private $app_pass;
  private $database;
  
  function __construct() {
    $this->app_user = getenv('FRUTAS_ADMIN_USER');
    $this->ip = getenv('FRUTAS_ADMIN_IP');
    $this->app_pass = getenv('FRUTAS_ADMIN_PASS');
    $this->database = getenv('FRUTAS_ADMIN_DATABASE');
    $conn_array = array(
    'ip' => $this->ip,
    'app_user' => $this->app_user,
    'app_pass' => $this->app_pass,
    'database' => $this->database
    );
    $this->conn = $this->crear_conexion($conn_array);    
  }

  function __destruct() {
    if(isset($this->conn) && !is_null($this->conn)) {
      // Llama al destructor
      $this->cerrar_conexion();
    }
  }

  public function crear_conexion($conn_params=array())
  {
    $conn = new mysqli($conn_params['ip'], $conn_params['app_user'], $conn_params['app_pass'], $conn_params['database']);
    if (!$conn) {
      die('Connect Error (' . mysqli_connect_errno() . ') '
    . mysqli_connect_error());
    }
    else{
      return $conn;
    }
  }

  public function cerrar_conexion()
  {
    mysqli_close($this->conn);
  }
  
  public function insertar_datos($query)
  {
    $query = $this->conn->real_escape_string($query);
    $result = $this->conn->query($query);
    return $result;
  }
  
  public function recuperar_datos($query)
  {
    //$query = $this->conn->real_escape_string($query);
    $result = $this->conn->query($query);
    return $result;
  }

  public function liberar_datos()
  {
    while($this->conn->more_results())
    {
      $this->conn->next_result();
      if($res = $this->conn->store_result()) // added closing bracket
      {
        $res->free(); 
      }
    }
  }
}

?>
