<?php

class Menu {

  private $menu_hash;

  function __construct($is_logged_in) {
     $this->menu_hash = array();
     $this->build_menu_hash($is_logged_in);
  }

  private function build_menu_hash($is_logged_in) {
    if($is_logged_in){
      $this->menu_hash["inventario"] = $GLOBALS['config']['site_url'] . '/config/routes.php?action=inventario';
    	$this->menu_hash["compras"] = $GLOBALS['config']['site_url'] . '/config/routes.php?action=compras';
      $this->menu_hash["ventas"] = $GLOBALS['config']['site_url'] . '/config/routes.php?action=ventas';
      $this->menu_hash["logout"] = $GLOBALS['config']['site_url'] . '/config/routes.php?action=logout';
      $this->menu_hash["agregar_frutas"] = $GLOBALS['config']['site_url'] . '/config/routes.php?action=comprar_frutas';
    }
  }

  public function get_menu_hash() {
    return $this->menu_hash;
  }

}
?>
