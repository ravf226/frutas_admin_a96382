<?php


include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_model.php');

class Sesion extends BaseModel {

  private $session_hash;
  private $usuario_id;

  function __construct() {
    if(session_id() == '' || !isset($_SESSION)) {
      session_start();
    }
    $this->session_hash = session_id();
    parent::__construct();
  }

  public function terminar_sesion() {
    if(session_id() != '' || isset($_SESSION)) {
    	//$_SESSION = array();
     	//session_destroy();
    }	
  }

  public function asociar_usuario($u_id) {
    $this->usuario_id = $u_id;
  }

  public function logged_in() {
    return false;
  }

  public function crear_sesion_activa($sesion_id, $usuario_id, $ip, $activa, $time_stamp) {
    $resultado = false;

    $q = "CALL insertar_en_sesion('". $sesion_id ."', '". $usuario_id ."', '". $ip ."', '". $activa ."', '". $time_stamp."')";
    if($result = $this->conexion->recuperar_datos($q)) {
      $resultado = true;
    }
    return $resultado;
  }

  public function actualizar_sesion_activa($sesion_id, $usuario_id, $ip, $activa, $time_stamp) {
    $resultado = false;
    $q = "CALL actualizar_en_sesion('". $sesion_id ."', '". $usuario_id ."', '". $ip ."', '". $activa ."', '". $time_stamp."')";
    if($result = $this->conexion->recuperar_datos($q)) {
      $resultado = true;
    }
    return $resultado;
  }

  public function borrar_sesion_activa() {
    $resultado = false;
    $q = "CALL borrar_sesion('". $this->session_hash ."')";
    if($result = $this->conexion->recuperar_datos($q)) {
      $resultado = true;
    } 
    return $resultado;
  }

  public function validar_sesion_activa($u_id) {
    $activa = false;
    $q = "CALL seleccionar_sesion('". $u_id ."')";
    if($result = $this->conexion->recuperar_datos($q)) {
      $row = $result->fetch_assoc();
      $tiempo = strtotime($row['fecha']);
      $tiempo_actual = time();
      if(($tiempo_actual - $tiempo) <= 180) {
        $activa = true;
      }
    }
    $result->free();
    $this->conexion->liberar_datos(); 
    return $activa;
  }	
}

?>
