<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_model.php');


class Usuario extends BaseModel {

  private $id;
  private $nombre;
  private $email;
    
  public function usuario_es_valido($usuario, $pass) {
    $usuario_sanitizado = filter_var($usuario, FILTER_SANITIZE_STRING);
    $pass_sanitizado = filter_var($pass, FILTER_SANITIZE_STRING);
    $resultado = false;
    $q = "CALL verificar_login('".$usuario_sanitizado."', '".$pass_sanitizado."')";
    if($result = $this->conexion->recuperar_datos($q)) {
      $row = $result->fetch_assoc();
      if( intval($row['COUNT(*)']) === 1 && filter_var($row['admin'], FILTER_VALIDATE_BOOLEAN)) { 
        //Se logueo exitosamente
        $resultado = true;
        $this->id = $row['id'];
        $this->nombre = $row['nombre'];
        $this->email = $row['email'];
      }             
    }

    return $resultado;
  }

  public function get_id() {
    return $this->id;
  }

}
?>
