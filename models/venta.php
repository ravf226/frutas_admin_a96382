<?php

include_once(dirname(__FILE__) . '/../configs.php');
include_once('base_model.php');

class Venta extends BaseModel  {
	
	public function get_ventas() {
      $results = array();
      $q = "CALL seleccionar_ventas()";
      $result = $this->conexion->recuperar_datos($q);
      while($row = $result->fetch_assoc()){         
        $results[] = $row;
      }
      $result->free();
      $this->conexion->liberar_datos(); 
      return $results;
	}
}
?>
