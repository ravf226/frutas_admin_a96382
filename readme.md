Este es un sistema en PHP puro (sin usar frameworks de ningun tipo) organizado usando el patron MVC.

Para configurar la base de datos:

1. Corra el script frutas_admin_a96382.sql

2. Corra el script inserts.sql

3. Corra el script stored_procedures.sql

4. Configurar las variables de ambiente necesarias en el servidor donde se va a probar la app.