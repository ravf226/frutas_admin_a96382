DELIMITER $$

USE usuario13$$

-- ---------------------INSERTAR-------------------------- 

-- sp insertar en tabla frutas

DROP PROCEDURE IF EXISTS insertar_en_frutas$$
CREATE PROCEDURE insertar_en_frutas (IN codigo varchar(250), 
										IN nombre varchar(250),
										IN cantidad INT,
                                        IN precio_unitario FLOAT)
begin

INSERT INTO `usuario13`.`frutas`
(`codigo`,
`nombre`,
`cantidad`,
`precio_unitario`)
VALUES
(codigo,
nombre,
cantidad,
precio_unitario);


end$$


-- sp insertar en tabla compras
DROP PROCEDURE IF EXISTS insertar_en_compras$$
CREATE PROCEDURE insertar_en_compras (IN codigo varchar(250), 
										IN fecha DATE,
										IN descripcion varchar(250),
                                        IN usuarios_id INT,
                                        IN frutas_id INT,
                                        IN monto FLOAT,
                                        informacion_pago varchar(500))
begin


INSERT INTO `usuario13`.`compras`
(`codigo`,
`fecha`,
`informacion_pago`,
`usuarios_id`,
`frutas_id`,
`descripcion`,
`monto`)
VALUES
(codigo,
fecha,
SHA1(informacion_pago),
usuarios_id,
frutas_id,
descripcion,
monto);


end$$

-- sp insertar en tabla ventas

DROP PROCEDURE IF EXISTS insertar_en_ventas$$
CREATE PROCEDURE insertar_en_ventas (IN codigo varchar(250), 
										IN fecha DATE,
										IN descripcion varchar(250),
                                        IN usuarios_id INT,
                                        IN frutas_id INT,
                                        IN monto FLOAT,
                                        informacion_pago varchar(500)
                                        )
begin

INSERT INTO `usuario13`.`ventas`
(`codigo`,
`fecha`,
`informacion_pago`,
`usuarios_id`,
`frutas_id`,
`descripcion`,
`monto`)
VALUES
(codigo,
fecha,
SHA1(informacion_pago),
usuarios_id,
frutas_id,
descripcion,
monto);



end$$

-- sp insertar en tabla bitacora

DROP PROCEDURE IF EXISTS insertar_en_bitacora$$

CREATE PROCEDURE insertar_en_bitacora (IN tipo_transaccion varchar(500), 
										IN codigo varchar(500),
										IN fecha timestamp,
                                        IN descripcion INT,
                                        IN usuarios_id INT
                                        )
begin

INSERT INTO `usuario13`.`bitacora`
(`tipo_transaccion`,
`codigo`,
`fecha`,
`descripcion`,
`usuarios_id`)
VALUES
(tipo_transaccion,
codigo,
fecha,
descripcion,
usuarios_id);

end$$

-- sp insertar en tabla sesiones

DROP PROCEDURE IF EXISTS insertar_en_sesion$$

CREATE PROCEDURE insertar_en_sesion (IN sesion varchar(500), 
										IN usuarios_id int,
										IN ip varchar(200),
                                        IN expira bool,
                                        IN fecha timestamp
                                        )
begin

INSERT INTO `usuario13`.`sesiones`
(`sesion`,
`usuarios_id`,
`ip`,
`expira`,
`fecha`)
VALUES
(sesion,
usuarios_id,
ip,
expira,
fecha);

end$$

-- sp insertar en tabla usuarios

DROP PROCEDURE IF EXISTS insertar_en_usuarios$$

 CREATE PROCEDURE insertar_en_usuarios (IN nombre varchar(250), 
										  IN pass varchar(250),
										  IN email varchar(250),
                                          IN direccion varchar(250),
                                          IN pregunta_seguridad varchar(1000),
                                          IN respuesta_seguridad varchar(1000),
                                          IN admin BOOL
                                          )
begin


INSERT INTO `usuario13`.`usuarios`
(`nombre`,
`password`,
`email`,
`direccion`,
`pregunta_seguridad`,
`respuesta_seguridad`,
`admin`)
VALUES
(nombre,
SHA1(pass),
email,
direccion,
pregunta_seguridad,
SHA1(respuesta_seguridad),
admin);

end$$
-- ---------------------UPDATEAR-------------------------- 

-- sp updatear en tabla frutas
DROP PROCEDURE IF EXISTS actualizar_fruta$$

CREATE PROCEDURE actualizar_fruta (IN fruta_id INT,	
									 IN codigo varchar(250),
									 IN nombre varchar(250),
									 IN cantidad INT,
                                     IN precio_unitario FLOAT
									 )

begin

UPDATE `usuario13`.`frutas`
SET
`codigo` = codigo,
`nombre` = nombre,
`cantidad` = cantidad,
precio_unitario = pr
WHERE `id` = fruta_id;

end$$

-- sp updatear en tabla ventas

DROP PROCEDURE IF EXISTS actualizar_ventas$$

CREATE PROCEDURE actualizar_ventas (IN venta_id INT,	
									 IN fecha DATE,
									 IN monto FLOAT,
                                     IN informacion_pago varchar(500),
                                     IN frutas_id INT,
                                     IN descripcion VARCHAR(250),
                                     IN usuarios_id INT
									 )

begin

UPDATE `usuario13`.`ventas`
SET
`fecha` = fecha,
`monto` = monto,
`informacion_pago` = informacion_pago,
`frutas_id` = frutas_id,
`descripcion` = descripcion,
`usuarios_id` = usuarios_id
WHERE `id` = venta_id;

end$$
-- sp updatear en tabla compras

DROP PROCEDURE IF EXISTS actualizar_compras$$

CREATE PROCEDURE actualizar_compras (IN compra_id INT,	
									 IN fecha DATE,
									 IN monto FLOAT,
                                     IN informacion_pago VARCHAR(500),
                                     IN frutas_id INT,
                                     IN descripcion VARCHAR(250),
                                     IN usuarios_id INT
									 )

begin

UPDATE `usuario13`.`compras`
SET
`fecha` = fecha,
`monto` = monto,
`informacion_pago` = informacion_pago,
`frutas_id` = frutas_id,
`descripcion` = descripcion,
`usuarios_id` = usuarios_id
WHERE `id` = compra_id;

end$$

-- sp updatear en tabla bitacora
DROP PROCEDURE IF EXISTS actualizar_en_bitacora$$

CREATE PROCEDURE actualizar_en_bitacora (IN tipo_transaccion varchar(500), 
										IN codigo varchar(500),
										IN fecha timestamp,
                                        IN descripcion INT,
                                        IN usuarios_id INT,
                                        IN bitacora_id INT
                                        )
begin

UPDATE `usuario13`.`bitacora`
SET
`tipo_transaccion` = tipo_transaccion,
`codigo` = codigo,
`fecha` = fecha,
`descripcion` = descripcion,
`usuarios_id` = usuarios_id
WHERE `id` = bitacora_id;

end$$

-- sp updatear en tabla sesiones

DROP PROCEDURE IF EXISTS actualizar_en_sesion$$

CREATE PROCEDURE actualizar_en_sesion(IN sesion varchar(500), 
									  IN usuarios_id int,
									  IN ip varchar(200),
									  IN expira bool,
									  IN fecha timestamp
									  )
begin
SET SQL_SAFE_UPDATES = 0;
UPDATE `usuario13`.`sesiones`
SET
`sesion` = sesion,
`usuarios_id` = usuarios_id,
`ip` = ip,
`expira` = expira,
`fecha` = fecha
WHERE `sesion` = sesion;

end$$

-- sp updatear en tabla usuarios
DROP PROCEDURE IF EXISTS actualizar_usuario$$
CREATE PROCEDURE actualizar_usuario (IN usuario_id INT,	
									 IN nombre varchar(250),
									 IN email varchar(250),
                                     IN direccion varchar(250),
									 IN pregunta varchar(1000),
									 IN respuesta varchar(1000),
									 IN contrasena varchar(250),
                                     IN admin BOOL
									 )

begin

UPDATE `usuario13`.`usuario`
SET
`nombre` = nombre,
`email` = email,
`direccion` = direccion,
`pregunta_seguridad` = pregunta,
`respuesta_seguridad` = SHA1(respuesta),
`password` = SHA1(contrasenna),
`admin` = admin
WHERE `id` = usuario_id;

end$$

-- ---------------------BORRAR-------------------------- 

-- sp borrar en tabla frutas
DROP PROCEDURE IF EXISTS borrar_frutas$$

CREATE PROCEDURE borrar_frutas (IN fruta_id INT)


begin

DELETE FROM `usuario13`.`frutas`
WHERE id = fruta_id;


end$$


-- sp borrar en tabla compras
DROP PROCEDURE IF EXISTS borrar_compras$$

CREATE PROCEDURE borrar_compras (IN compra_id INT)


begin

DELETE FROM `usuario13`.`compras`
WHERE id = compra_id;


end$$

-- sp borrar en tabla ventas
DROP PROCEDURE IF EXISTS borrar_ventas$$


CREATE PROCEDURE borrar_ventas (IN usuario_id INT)


begin

DELETE FROM `usuario13`.`usuarios`
WHERE id = usuario_id;


end$$

-- sp borrar en tabla bitacora
DROP PROCEDURE IF EXISTS borrar_bitacora$$


CREATE PROCEDURE borrar_bitacora (IN bitacora_id INT)


begin

DELETE FROM `usuario13`.`bitacora`
WHERE id = bitacora_id;


end$$

-- sp borrar en tabla sesion
DROP PROCEDURE IF EXISTS borrar_sesion$$


CREATE PROCEDURE borrar_sesion (IN sesion_id VARCHAR(500))


begin

DELETE FROM `usuario13`.`sesiones`
WHERE sesion = sesion_id;


end$$

-- ------------------------ SP SELECTS -------------------------------

DROP PROCEDURE IF EXISTS verificar_login$$


CREATE PROCEDURE verificar_login (IN nombre_usuario varchar(250),
							      IN pass_usuario varchar(250))


begin

SELECT COUNT(*), u.nombre, u.email, u.direccion, u.id, u.admin
FROM `usuarios` u
WHERE u.`nombre` = nombre_usuario
AND u.`password` = SHA1(pass_usuario);

end$$

DROP PROCEDURE IF EXISTS seleccionar_compras$$
CREATE PROCEDURE seleccionar_compras ()
begin

SELECT c.*, f.*
FROM `compras` c, `frutas` f
WHERE f.id = c.frutas_id;

end$$


DROP PROCEDURE IF EXISTS seleccionar_ventas$$
CREATE PROCEDURE seleccionar_ventas ()
begin

SELECT v.*, f.*
FROM `ventas` v, `frutas` f
WHERE f.id = v.frutas_id;

end$$

DROP PROCEDURE IF EXISTS seleccionar_ventas_de_usuario$$
CREATE PROCEDURE seleccionar_ventas_de_usuario (IN u_id INT)
begin

SELECT v.*, f.*
FROM `ventas` v, `frutas` f
WHERE f.id = v.frutas_id
AND v.usuarios_id = u_id;

end$$

DROP PROCEDURE IF EXISTS seleccionar_inventario$$
CREATE PROCEDURE seleccionar_inventario ()
begin

SELECT v.*, f.*, c.*
FROM `ventas` v, `frutas` f, `compras` c 
WHERE f.id = v.frutas_id  
AND f.id = c.frutas_id;

end$$


DROP PROCEDURE IF EXISTS lista_frutas$$
CREATE PROCEDURE lista_frutas ()
begin

SELECT f.*
FROM `frutas` f;

end$$

DROP PROCEDURE IF EXISTS seleccionar_sesion$$
CREATE PROCEDURE seleccionar_sesion (IN usuario_id INT)
begin

SELECT s.*
FROM `sesiones` s
WHERE s.usuarios_id = usuario_id;

end$$

DROP PROCEDURE IF EXISTS actualizar_seguro$$
CREATE PROCEDURE actualizar_seguro ()
begin
  SET SQL_SAFE_UPDATES = 0;
end$$
