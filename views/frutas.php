<?php include 'partials/_header.php';?>

<div>
  <div id="mensajes">
      <?php 
      if(isset($_SESSION['mensajes']) && count($_SESSION['mensajes']) > 0) {
        echo '<div id="alert">';
        foreach ($_SESSION['mensajes'] as $key => $value) {
          if($key != '') {
            echo "<p class='alert'>" . $value . "</p>";
          }
        }
        echo '</div></br></br></br></br></br>';
      }
    ?>
  </div>
  <form id="login_form" action="../config/routes.php" method="post">
    <div class='frutas'>
    <input  type="hidden" name="action" value="procesar_compra_frutas">
    <div>
    	<!-- DROPDOWN con info de frutas -->
    	<select id="select_fruta" name="select_fruta">
    	<?php 
         foreach ($_SESSION['frutas'] as $key => $value) {
         	# code...
            echo '<option value="'.$value['id'].'|'. $value['precio_unitario'] . '" id="option_user">'.$value['nombre'] .'| Valor: ' . $value['precio_unitario'] .  '</option>';
         }
    	?>
    	</select>
    </div>
    <br/>
    <div>  
      <input type="number" min="1" name="cantidad_fruta" id="cantidad_fruta"/>
      <label for="cantidad_fruta">Digite la cantidad de fruta.</label>
    </div>
    <br/>
    <div>  
      <input type="text" value="" placeholder="Codigo de Compra" name="codigo_fruta" id="codigo_fruta"/>
      <label for="codigo_fruta">Digite el codigo de la compra.</label>
    </div>
    <br/>
    <div>  
      <input type="text" value="" placeholder="Desc de Compra" name="desc" id="desc"/>
      <label for="desc">Digite la desc de la compra.</label>
    </div>
	  <br/>
	 <div>  
      <input type="text" value="" placeholder="ejm, 11257895123" name="numero_tarjeta" id="numero_tarjeta"/>
      <label for="numero_tarjeta">Digite # de tarjeta.</label>
    </div>
    <br/>
    <p>Digite la fecha de vencimiento de la tarjeta</p>	
	   <div>  
      <input type="numeric" value="" name="venc_tarjeta_dia" id="venc_tarjeta_dia" placeholder="23"/>
      <label for="venc_tarjeta_dia">DD</label>
    </div>
    <div>  
      <input type="numeric" value="" name="venc_tarjeta_mes" id="venc_tarjeta_mes" placeholder="07"/>
      <label for="venc_tarjeta_mes">MM</label>
    </div>
    <div>  
      <input type="numeric" value="" name="venc_tarjeta_year" id="venc_tarjeta_year" placeholder="2018"/>
      <label for="venc_tarjeta_year">YYYY</label>
    </div>
      <br/>
	   <div>  
      <input type="password" value="" placeholder="ejm, 718" name="codigo_tarjeta" id="codigo_tarjeta"/>
      <label for="codigo_tarjeta">Digite Codigo de seguridad de Tarjeta.</label>
    </div>
    <button type="submit">Comprar</button>
    </div>
  </form>
</div>

<?php include 'partials/_footer.php';?>
