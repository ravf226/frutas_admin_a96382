<?php include 'partials/_header.php';?>


<div>
<h1>Inventario de Frutas</h1>
<p>El inventario permite revisar las compras menos las ventas.</p>
  <div class="wrapper">
    <div class="row header">
      <div class="cell">
        Fruta
      </div>
      <div class="cell">
        Cantidad en Inventario
      </div>
      <div class="cell">
        Precio Unitario
      </div>
      <div class="cell">
        Total de la Compra
      </div>
      <div class="cell">
        Descripcion de la Compra
      </div>
    </div>

      <?php 
        foreach ($_SESSION['inventario'] as $key => $value) {
          # code...
          echo '<div class="row">';
          echo '<div class="cell">' . $value['nombre'] . '</div>';
          echo '<div class="cell">' . ($value['monto']/$value['precio_unitario']) . '</div>';
          echo '<div class="cell">' . $value['precio_unitario'] . '</div>';
          echo '<div class="cell">' . $value['monto'] . '</div>';
          echo '<div class="cell">' . $value['descripcion'] . '</div>';
          echo '</div>';
        }
      ?>
 
    
  </div>
  
</div>


<?php include 'partials/_footer.php';?>


<?php 



?>
