<?php session_start(); ?>
<?php include_once(dirname(__FILE__) . '/../configs.php'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
  <title>Frutas Admin</title>
   <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['config']['site_url'] . '/assets/stylesheet/general.css' ?>">
</head>
<body>
  <header>
    <span><h1>Frutas A96382</h1></span>
    <nav id="navigation">
    </nav>
  </header>
<div>
  <div id="mensajes">
    
    <?php 
      if(isset($_SESSION['mensajes']) && count($_SESSION['mensajes']) > 0) {
        echo '<div id="alert">';
        foreach ($_SESSION['mensajes'] as $key => $value) {
          if($key != '') {
            echo "<p class='alert'>" . $value . "</p>";
          }
        }
        echo '</div></br></br></br></br></br>';
      }
    ?>

  </div>
  <form id="login_form" action="../config/routes.php" method="post">
    <div class='login'>
    <input  type="hidden" name="action" value="login">
    <div>
      <input type="text" value="" placeholder="Usuario" name="user_name" id="login_name"/>
      <label for="login_name">Digite el nombre de usuario.</label>
    </div>
    <br/>
    <div>  
      <input type="password" value="" placeholder="Password" name="user_pass" id="login_pass"/>
      <label for="pass">Digite el password.</label>
    </div>
	  <br/>
    <button type="submit">Entrar</button>
    </div>
  </form>
</div>

<?php include 'partials/_footer.php';?>
