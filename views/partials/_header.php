<?php session_start(); ?>
<?php include_once(dirname(__FILE__) . '/../../configs.php'); ?>
<?php include_once('_sesion.php'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
  <title>Frutas Admin</title>
  <!-- css -->
  <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['config']['site_url'] . '/assets/stylesheet/table.css' ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['config']['site_url'] . '/assets/stylesheet/general.css' ?>">
</head>
<body>
  <header>
  	<span>Frutas Admin A96382</span>
  	<nav id="navigation">
  		<?php include_once('_menu.php'); ?>
  	</nav>
  </header>
