<?php 

include_once(dirname(__FILE__) . '/../../controllers/sesion_controller.php');
include_once(dirname(__FILE__) . '/../../controllers/menu_controller.php');

$sesion_controller = new SesionController();
$menu_controller = new MenuController($sesion_controller->sesion_activa($_SESSION['uid']));

?>



<ul>
<?php 
  if($sesion_controller->sesion_activa($_SESSION['uid'])){ 
  	$menu_item = $menu_controller->get_menu();
    foreach ($menu_item as $key => $value) {
      echo '<li><a href="'.$value.'">'.$key.'</a></li>';
    }
   
  }

?>


	
</ul>
