<?php 

//  Si esta logueado retorna vacio, si sesion esta vencida redireccion a login
// con mensaje de error

include_once(dirname(__FILE__) . '/../../configs.php');
include_once(dirname(__FILE__) . '/../../controllers/sesion_controller.php');
include_once(dirname(__FILE__) . '/../../controllers/login_controller.php');

$sesion_controller = new SesionController();

if(!$sesion_controller->sesion_activa($_SESSION['uid'])) {
  // destruir todo
  $sesion_controller->borrar_sesion_activa(session_id());
  $sesion_controller->clean_all_but_messages($_SESSION);
  $mensajes = array('warning' => '', 'error' => 'Su sesion ha expirado. Por favor reingrese sus credenciales.');
  $login_controller = new LoginController(false, $mensajes);
  //header("Location: http://localhost:8000");
  die();
}
else {
	// si esta activa updateo la sesion
   $t = date("Y-m-d H:i:s", time());
   $sesion_controller->actualizar_sesion_activa(session_id(), $_SESSION['uid'], $_SERVER['REMOTE_ADDR'], 0, $t);
}

?>
