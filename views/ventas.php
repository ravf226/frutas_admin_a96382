<?php include 'partials/_header.php';?>

<div>
<h1>Venta de Frutas</h1>
 <div class="wrapper">
    <div class="row header green">
      <div class="cell">
        Fruta
      </div>
      <div class="cell">
        Cantidad Vendida
      </div>
      <div class="cell">
        Precio Unitario
      </div>
      <div class="cell">
        Total de la Venta
      </div>
      <div class="cell">
        Descripcion de la Venta
      </div>
    </div>

      <?php 
        foreach ($_SESSION['ventas'] as $key => $value) {
          # code...
          echo '<div class="row">';
          echo '<div class="cell">' . $value['nombre'] . '</div>';
          echo '<div class="cell">' . ($value['monto']/$value['precio_unitario']) . '</div>';
          echo '<div class="cell">' . $value['precio_unitario'] . '</div>';
          echo '<div class="cell">' . $value['monto'] . '</div>';
          echo '<div class="cell">' . $value['descripcion'] . '</div>';
          echo '</div>';
        }
      ?>
 
    
  </div>
</div>


<?php include 'partials/_footer.php';?>


<?php 



?>
